package application;

import java.util.ArrayList;

import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Pane;

public class ResultController {
	
	@FXML
	private Pane p;
	@FXML
	private ScrollPane sp;
	@FXML
	private Group h;
	@FXML
	private Button newP;
	
	private Label pathsLabel = new Label();
	private Label pathsHead = new Label();
	private Label loopsLabel = new Label();
	private Label loopsHead = new Label();
	private Label unTouchedLoopsLabel = new Label();
	private Label unTouchedLoopsHead = new Label();
	private Label result = new Label();
	private double finalAnswer;
	private SignalFlowGraph signalFlowGraph = new SignalFlowGraph();
	private ArrayList<String> loops;
	private ArrayList<String> paths;
	ArrayList<ArrayList<Integer>>  unTouchedLoops;
	
	public void showResult() {
		finalAnswer = signalFlowGraph.SolveGraph();
		loops = signalFlowGraph.getLoops();
		paths = signalFlowGraph.getPaths();
		unTouchedLoops = signalFlowGraph.getUntouchedLoops();
		pathsHead.setText("Paths:");
		pathsHead.setLayoutX(40);
		pathsHead.setLayoutY(500);
		pathsLabel.setLayoutX(100);
		pathsLabel.setLayoutY(500);
		loopsHead.setText("Loops:");
		loopsHead.setLayoutX(40);
		loopsHead.setLayoutY(550);
		loopsLabel.setLayoutX(100);
		loopsLabel.setLayoutY(550);
		unTouchedLoopsHead.setText("Untouched Loops:");
		unTouchedLoopsHead.setLayoutX(40);
		unTouchedLoopsHead.setLayoutY(600);
		unTouchedLoopsLabel.setLayoutX(160);
		unTouchedLoopsLabel.setLayoutY(600);
		result.setText("Result: " + finalAnswer);
		result.setLayoutX(40);
		result.setLayoutY(650);
		
		for(int i = 0; i < paths.size(); i++) {
			String temp = "M" + i + ": ";
			if(i != 0) {
				pathsLabel.setText(pathsLabel.getText() + " / " + temp + paths.get(i));
			}
			else {
				pathsLabel.setText(temp + paths.get(i));
			}
		}
		for(int i = 0; i < loops.size(); i++) {
			String temp = "L" + i + ": ";
			if(i != 0) {
				loopsLabel.setText(loopsLabel.getText() + " / " + temp + loops.get(i));
			}
			else {
				loopsLabel.setText(temp + loops.get(i));
			}
		}
		for(int i = 0; i < unTouchedLoops.size(); i++) {
			String temp = "";
			for(int j = 0; j < unTouchedLoops.get(i).size(); j++) {
				temp += "L";
				temp += unTouchedLoops.get(i).get(j) + " ";
			}
			if(i != 0) {
				unTouchedLoopsLabel.setText(unTouchedLoopsLabel.getText() + " / " + temp);
			}
			else {
				unTouchedLoopsLabel.setText(temp);
			}
		}
		
		
		
		
	}
}
