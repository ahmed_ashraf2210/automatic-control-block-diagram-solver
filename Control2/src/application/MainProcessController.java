package application;

import java.awt.Font;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcTo;
import javafx.scene.shape.Circle;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Polygon;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class MainProcessController {
	@FXML
	private ChoiceBox<String> c1;
	@FXML
	private Button b;
	@FXML
	private Button n;
	@FXML
	private Button b1;
	@FXML
	private Button showGraph;
	@FXML
	private Button bResult;
	@FXML
	private ChoiceBox<String> c2;
	@FXML
	private Pane p;
	@FXML
	private Label l1;
	@FXML
	private Label l2;
	@FXML
	private Label l3;
	@FXML
	private Group h2;
	@FXML
	private Pane master;
	@FXML
	private ScrollPane sp;
	@FXML
	private Group h;
	@FXML
	private TextField weightField;
	
	private double finalAnswer;
	private SignalFlowGraph signalFlowGraph = new SignalFlowGraph();
	private ArrayList<Double> delta;
	private int numOfNodes;
	private Label w;
	private Label pathsLabel = new Label();
	private Label pathsHead = new Label();
	private Label loopsLabel = new Label();
	private Label loopsHead = new Label();
	private Label unTouchedLoopsLabel = new Label();
	private Label unTouchedLoopsHead = new Label();
	private Label result = new Label();
	private int radius = 15;
	private int centerx;
	private int centery;
	private String node1;
	private String node2;
	private double weight;
	private ArrayList<Integer> points = new ArrayList<>();
	private ArrayList<String> loops;
	private ArrayList<String> paths;
	ArrayList<ArrayList<Integer>>  unTouchedLoops;
	
	public void setNumOfNodes(int numOfNodes) {
		this.numOfNodes = numOfNodes;
		n.managedProperty().bind(n.visibleProperty());
		n.setVisible(false);
		h.getChildren().add(pathsHead);
		h.getChildren().add(pathsLabel);
		h.getChildren().add(loopsHead);
		h.getChildren().add(loopsLabel);
		h.getChildren().add(unTouchedLoopsLabel);
		h.getChildren().add(unTouchedLoopsHead);
		h.getChildren().add(result);
		sp.setContent(h);
		signalFlowGraph.setNumberOfNodes(numOfNodes);
		for (int i = 0; i < numOfNodes; i++) {
			c1.getItems().add(i + "");
			c2.getItems().add(i + "");
		}
		c1.getSelectionModel().select(0);
		c2.getSelectionModel().select(0);
		for (int i = 0; i < numOfNodes; i++) {
			Circle circle = new Circle(centerx, centery, radius);
			circle.setFill(Color.GRAY);
			Text text = new Text("X" + i);
			StackPane stack = new StackPane();
			stack.getChildren().addAll(circle, text);
			stack.setLayoutX(50 + i * 100);
			stack.setLayoutY(180);
			h.getChildren().add(stack);
			sp.setContent(h);
	
			Path path = new Path();
			MoveTo moveTo = new MoveTo();
			moveTo.setX(0);
			moveTo.setY(0);
			LineTo lineTo = new LineTo();
			lineTo.setX(0);
			lineTo.setY(370);
			path.getElements().add(moveTo);
			path.getElements().add(lineTo);
			h.getChildren().add(path);
			sp.setContent(h);
			
			int temp = 0;
			points.add(temp);
		}
	}

	public void drawNodes() {
		node1 = c1.getSelectionModel().getSelectedItem();
		node2 = c2.getSelectionModel().getSelectedItem();
		int n1 = Integer.parseInt(node1);
		int n2 = Integer.parseInt(node2);
		int t1 = n1;
		int t2 = n2;
		int z;
		
		try {
			weight = Double.parseDouble(weightField.getText());
		}
		catch (Exception e) {
			// TODO: handle exception
			weightField.clear();
			weightField.setStyle("-fx-border-color: red;");
			weightField.setPromptText("Please Insert Correct Number!");
			return;
		}
		if(weight == 0) {
			weightField.clear();
			weightField.setStyle("-fx-border-color: red;");
			weightField.setPromptText("Please Insert Correct Number!");
			return;
		}
		w = new Label();
		w.setText(weight + "");
		if(n2 < n1) {
			z = n1;
			n1 = n2;
			n2 = z;
		}
		if (t2 - t1 == 1) {
			line(n1, t2, w);
		}
		else if(t2 - t1 == 0) {
			loop(n1, w);
		}
		else {
			if (t1 < t2) {
				upperArc(n1, n2, t2, w);
				points.add(n1, points.remove(n1) + 1);
				points.add(n2, points.remove(n2) + 1);
			} else {
				lowerArc(n1, n2, t2, w);
				points.add(n1, points.remove(n1) + 1);
				points.add(n2, points.remove(n2) + 1);
			}
		}
		signalFlowGraph.addEdge(t1, t2, weight);
	}

	private void upperArc(int n1, int n2, int t2, Label w) {
		Path path = new Path();
		MoveTo moveTo = new MoveTo();
		moveTo.setX(65 + (n1 * 100) + 100 * (Math.abs(n1 - n2)));
		moveTo.setY(180);
		ArcTo arcTo = new ArcTo();
		arcTo.setX(65 + (n1 * 100));
		arcTo.setY(180);
		
		arcTo.setRadiusX(Math.abs(n1 - n2) * 15);
		arcTo.setRadiusY(Math.abs(n1 - n2) * 5);
		
		path.getElements().add(moveTo);
		path.getElements().add(arcTo);
		h.getChildren().add(path);
		sp.setContent(h);
		 Polygon triangle = new Polygon();
		 double x = 65 + (t2 * 100)-5;
		 double y = 180-5;
		 triangle.getPoints().addAll(new Double[]{
	            x, y,
	            x+9.0, y,
	            x+5.0, y+5.0 });
		 h.getChildren().add(triangle);
			sp.setContent(h);
			w.setLayoutX(x);
			w.setLayoutY(y-20);
			h.getChildren().add(w);
			sp.setContent(h);
	}

	private void lowerArc(int n1, int n2, int t2, Label w) {
		Path path = new Path();
		MoveTo moveTo = new MoveTo();
		moveTo.setX(65 + (n1 * 100));
		moveTo.setY(210);
		ArcTo arcTo = new ArcTo();
		arcTo.setX(65 + (n1 * 100) + 100 * (Math.abs(n1 - n2)));
		arcTo.setY(210);
		
		arcTo.setRadiusX(Math.abs(n1 - n2) * 15);
		arcTo.setRadiusY(Math.abs(n1 - n2) * 5);
		
		path.getElements().add(moveTo);
		path.getElements().add(arcTo);
		h.getChildren().add(path);
		sp.setContent(h);
		Polygon triangle = new Polygon();
		double x = 65 + (t2 * 100)-4;
		double y = 215;
		triangle.getPoints().addAll(new Double[]{
				x, y,
	            x+9.0, y,
	            x+5.0, y-5.0 });
		h.getChildren().add(triangle);
		sp.setContent(h);
		w.setLayoutX(x);
		w.setLayoutY(y+7);
		h.getChildren().add(w);
		sp.setContent(h);
	}

	private void line(int n1, int t2, Label w) {
		Path path = new Path();
		MoveTo moveTo = new MoveTo();
		moveTo.setX(80 + (n1 * 100));
		moveTo.setY(195);
		LineTo lineTo = new LineTo();
		lineTo.setX(80 + (n1 * 100) + 70);
		lineTo.setY(195);
		path.getElements().add(moveTo);
		path.getElements().add(lineTo);
		h.getChildren().add(path);
		sp.setContent(h);
		
		Polygon triangle = new Polygon();
		 double x = 80 + (n1 * 100) + 60;
		 double y = 195-5;
		 triangle.getPoints().addAll(new Double[]{
	            x, y,
	            x+10.0, y+5,
	            x, y+10.0 });
		 
		 h.getChildren().add(triangle);
		 sp.setContent(h);
		 w.setLayoutX(x-15);
			w.setLayoutY(y-15);
			h.getChildren().add(w);
			sp.setContent(h);
	}
	
	private void loop(int n1, Label w) {
		Path path = new Path();
		
		MoveTo moveTo = new MoveTo();
		moveTo.setX(65 + (n1 * 100));
		moveTo.setY(180);
		ArcTo arcTo = new ArcTo();
		arcTo.setX(65 + (n1 * 100) );
		arcTo.setY(127);
		
		arcTo.setRadiusX(20);
		arcTo.setRadiusY(20);
		
		path.getElements().add(moveTo);
		path.getElements().add(arcTo);
		h.getChildren().add(path);
		
		sp.setContent(h);
		Path path1 = new Path();
		
		MoveTo moveTo1 = new MoveTo();
		moveTo1.setX(65 + (n1 * 100));
		moveTo1.setY(127);
		ArcTo arcTo1 = new ArcTo();
		arcTo1.setX(65 + (n1 * 100) );
		arcTo1.setY(177);
		
		arcTo1.setRadiusX(20);
		arcTo1.setRadiusY(20);
		
		path.getElements().add(moveTo1);
		path.getElements().add(arcTo1);
		h.getChildren().add(path1);
		
		sp.setContent(h);
		 Polygon triangle = new Polygon();
		 double x = 65 + (n1 * 100)-5;
		 double y = 180-5;
		 triangle.getPoints().addAll(new Double[]{
	            x, y,
	            x+9.0, y,
	            x+5.0, y+5.0 });
		 h.getChildren().add(triangle);
			sp.setContent(h);
			w.setLayoutX(x);
			w.setLayoutY(y-15);
			h.getChildren().add(w);
			sp.setContent(h);
	}
	public void showResult() {
		c1.managedProperty().bind(c1.visibleProperty());
		c1.setVisible(false);
		c2.managedProperty().bind(c2.visibleProperty());
		c2.setVisible(false);
		weightField.managedProperty().bind(weightField.visibleProperty());
		weightField.setVisible(false);
		b.managedProperty().bind(b.visibleProperty());
		b.setVisible(false);
		l1.managedProperty().bind(l1.visibleProperty());
		l1.setVisible(false);
		l2.managedProperty().bind(l2.visibleProperty());
		l2.setVisible(false);
		l3.managedProperty().bind(l3.visibleProperty());
		l3.setVisible(false);
		n.managedProperty().bind(n.visibleProperty());
		n.setVisible(true);
		finalAnswer = signalFlowGraph.SolveGraph();
		loops = signalFlowGraph.getLoops();
		paths = signalFlowGraph.getPaths();
		delta = signalFlowGraph.getDelta();
		unTouchedLoops = signalFlowGraph.getUntouchedLoops();
		pathsHead.setText("Paths:");
		pathsHead.setLayoutX(40);
		pathsHead.setLayoutY(500);
		pathsLabel.setLayoutX(100);
		pathsLabel.setLayoutY(500);
		loopsHead.setText("Loops:");
		loopsHead.setLayoutX(40);
		loopsHead.setLayoutY(550);
		loopsLabel.setLayoutX(100);
		loopsLabel.setLayoutY(550);
		unTouchedLoopsHead.setText("Untouched Loops:");
		unTouchedLoopsHead.setLayoutX(40);
		unTouchedLoopsHead.setLayoutY(600);
		unTouchedLoopsLabel.setLayoutX(160);
		unTouchedLoopsLabel.setLayoutY(600);
		result.setText("Result: " + finalAnswer);
		result.setLayoutX(40);
		result.setLayoutY(650);
		
		for(int i = 0; i < paths.size(); i++) {
			String temp = "M" + i + ": ";
			if(i != 0) {
				pathsLabel.setText(pathsLabel.getText() + " / " + temp + paths.get(i));
			}
			else {
				pathsLabel.setText(temp + paths.get(i));
			}
		}
		for(int i = 0; i < loops.size(); i++) {
			String temp = "L" + i + ": ";
			if(i != 0) {
				loopsLabel.setText(loopsLabel.getText() + " / " + temp + loops.get(i));
			}
			else {
				loopsLabel.setText(temp + loops.get(i));
			}
		}
		for(int i = 0; i < unTouchedLoops.size(); i++) {
			String temp = "";
			for(int j = 0; j < unTouchedLoops.get(i).size(); j++) {
				temp += "L";
				temp += unTouchedLoops.get(i).get(j) + " ";
			}
			if(i != 0) {
				unTouchedLoopsLabel.setText(unTouchedLoopsLabel.getText() + " / " + temp);
			}
			else {
				unTouchedLoopsLabel.setText(temp);
			}
		}
		
		for(int i = 0; i < delta.size(); i++) {
			Label l = new Label();
			l.setLayoutX(40);
			l.setLayoutY(700 + i*30);
			h.getChildren().add(l);
			sp.setContent(h);
			String s = "DELTA ";
			if(i != 0) {
				s = s + i;
			}
			s = s + ": " + delta.get(i);
			l.setText(s);
		}
		
		
		
		
	}
	public void goNextScene() throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("numberOfNodes.fxml"));
    	Parent root = loader.load();
    	NumberOfNodesController numberOfNodesController = loader.getController();
		Scene scene = new Scene(root,341,220);
		Stage primaryStage = (Stage) n.getScene().getWindow();
		primaryStage.setScene(scene);
		
    }
}
