package application;

import java.util.ArrayList;

public class SignalFlowGraph {
	private int numberOfNodes;
	private double[][] graph;
	private ArrayList<Double> pathWeight;
	private ArrayList<String> loops;
	private ArrayList<Double> loopWeight;
	private boolean[] visited;
	private ArrayList<String> paths;
	private ArrayList<ArrayList<Integer>> untouchedLoops;
	private ArrayList<Double> delta;
	public ArrayList<String> getPaths() {
		return paths;
	}

	public ArrayList<String> getLoops() {
		return loops;
	}
	
	public ArrayList<Double> getDelta() {
		return delta;
	}

	public ArrayList<ArrayList<Integer>> getUntouchedLoops() {
		return untouchedLoops;
	}
	
	public int getNumberOfNodes() {
		return numberOfNodes;
	}

	public void setNumberOfNodes(int numberOfNodes) {
		this.numberOfNodes = numberOfNodes;
		graph = new double[numberOfNodes][numberOfNodes];
	}

	public double SolveGraph() {
		loops = new ArrayList<>();
		paths = new ArrayList<>();
		loopWeight = new ArrayList<>();
		pathWeight = new ArrayList<>();
		untouchedLoops = new ArrayList<>();
		delta = new ArrayList<>();
		visited = new boolean[numberOfNodes];
		solve(0, "", 1);
		double numerator = 0;
		double dominator = delta(loops, loopWeight, 0, 0);
		delta.add(dominator);
		for (int i = 0; i < paths.size(); i++) {
			ArrayList<Double> x = new ArrayList<>();
			delta.add((double) delta(getUntochedLoops(x, paths.get(i)), x, 0, 0));
			numerator += delta.get(delta.size() - 1) * pathWeight.get(i);
			
		}
		return numerator / dominator;
	}

	public void addEdge(int from, int to, double weight) {
		graph[from][to] = weight;
	}

	private void addLoop(String path) {
		char l = path.charAt(path.length() - 1);
		path = path.substring(1, path.length() - 1);
		path = path.substring(path.lastIndexOf(l));
		path += l;
		for (int i = 0; i < loops.size(); i++) {
			if (isSameLoop(path, loops.get(i))) {
				return;
			}
		}
		double weight = 1;
		String[] nodes = path.split(" ");
		int prev = Integer.parseInt(nodes[0]);
		for (int i = 1; i < nodes.length; i++) {
			int next = Integer.parseInt(nodes[i]);
			weight *= graph[prev][next];
			prev = next;
		}
		loops.add(path);
		loopWeight.add(weight);
	}

	private boolean isSameLoop(String loop1, String loop2) {
		loop1 = loop1.substring(0, loop1.length() - 2);
		loop2 = loop2.substring(0, loop2.length() - 2);
		if (loop1.length() != loop2.length()) {
			return false;
		}
		int j = loop2.indexOf(loop1.charAt(0) + "");
		if (j == -1) {
			return false;
		}
		for (int i = 0; i < loop1.length();) {
			if (loop1.charAt(i) == ' ') {
				i++;
			}
			if (loop2.charAt(j) == ' ') {
				j = (j + 1) % loop1.length();
			}
			if (loop1.charAt(i) != loop2.charAt(j)) {
				return false;
			}
			i++;
			j = (j + 1) % loop1.length();
		}
		return true;
	}

	private void solve(int index, String path, double weight) {
		visited[index] = true;
		path += " " + index;
		if (index == graph.length - 1) {
			paths.add(path.substring(1, path.length()));
			pathWeight.add(weight);
		}
		for (int i = 0; i < graph.length; i++) {
			if (graph[index][i] != 0) {
				if (!visited[i]) {
					solve(i, path, weight * graph[index][i]);
				} else {
					addLoop(path + " " + i);
				}
			}
		}

		visited[index] = false;
	}

	private double delta(ArrayList<String> loopName, ArrayList<Double> loopgain, int index, long mask) {
		if (index == loopName.size()) {
			if (loopName == loops) {
				int freq = 0;
				ArrayList<Integer> x = new ArrayList<>();
				for (int i = 0; i < loopName.size(); i++) {
					if (((mask >> i) & 1) != 0) {
						x.add(i+1);
						freq++;
					}
				}
				if (freq > 1) {
					untouchedLoops.add(x);
				}
			}
			return 1;
		}
		long c = 0;
		for (int i = 0; i < index; i++) {
			if (((mask >> i) & 1) != 0) {
				if (isTouched(loopName.get(index), loopName.get(i))) {
					return delta(loopName, loopgain, index + 1, mask);
				}
			}
		}
		return delta(loopName, loopgain, index + 1, mask)
				- loopgain.get(index) * delta(loopName, loopgain, index + 1, mask | 1 << index);
	}

	private boolean isTouched(String path1, String path2) {
		for (int i = 0; i < path1.length(); i++) {
			if (path1.charAt(i) == ' ') {
				continue;
			}
			if (path2.contains("" + path1.charAt(i))) {
				return true;
			}
		}
		return false;
	}

	private ArrayList<String> getUntochedLoops(ArrayList<Double> loopsGain, String path) {
		ArrayList<String> loops = new ArrayList<>();
		for (int i = 0; i < this.loops.size(); i++) {
			if (isTouched(this.loops.get(i), path)) {

			} else {
				loops.add(this.loops.get(i));
				loopsGain.add(loopWeight.get(i));
			}
		}
		return loops;
	}

	public static void main(String[] args) {
		SignalFlowGraph x = new SignalFlowGraph();
		x.setNumberOfNodes(6);
		x.addEdge(0, 1, 1);
		x.addEdge(1, 2, 1);
		x.addEdge(1, 3, 1);
		x.addEdge(1, 4, 1);
		x.addEdge(2, 1, -1);
		x.addEdge(2, 3, 1);
		x.addEdge(3, 2, -1);
		x.addEdge(3, 4, 1);
		x.addEdge(3, 3, -1);
		x.addEdge(4, 5, 1);
		System.out.println(x.SolveGraph());
		

	}

}
