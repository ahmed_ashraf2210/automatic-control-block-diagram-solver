package application;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class NumberOfNodesController {

    @FXML
    private TextField t;
    
    @FXML
    private Button b;
    
    private int nOfNodes;

    @FXML
    void getNumberOfNodes(ActionEvent event) {
    	String temp = t.getText();
    	
    	try {
    		nOfNodes = Integer.parseInt(temp);
    		t.setStyle("-fx-border-color: black;");
    		goNextScene();
		} catch (Exception e) {
			// TODO: handle exception
			t.clear();
			t.setStyle("-fx-border-color: red;");
			t.setPromptText("Please Insert Correct Number!");
		}
    }
    private void goNextScene() throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("MainProcess.fxml"));
    	Parent root = loader.load();
    	MainProcessController mainProcessController = loader.getController();
		Scene scene = new Scene(root,722,543);
		mainProcessController.setNumOfNodes(nOfNodes);
		Stage primaryStage = (Stage) t.getScene().getWindow();
		primaryStage.setScene(scene);
    }
}
